public class Item
{
	private String itemName;
	private int itemID, userID;
	private Location locHistory;
	private boolean isLost, isArchived;
	
	public Item()
	{
		itemName = "";
		itemID = -1;
		userID = -1;
		locHistory = new Location();
		isLost = false;
		isArchived = false;
	}
	
	public Item(String name, int itemNum, int userNum)
	{
		itemName = name;
		itemID = itemNum;
		userID = userNum;
		locHistory = new Location();
		isLost = false;
		isArchived = false;
	}
	
	public Item(int itemID, int userID, String name, boolean lost, boolean archived)
	{
		this.itemID = itemID;
		this.userID = userID;
		itemName = name;
		isLost = lost;
		isArchived = archived;
		locHistory = new Location();
	}
	
	public String getItemName()
	{
		return itemName;
	}
	
	public int getItemID()
	{
		return itemID;
	}
	
	public int getUserID()
	{
		return userID;
	}
	
	public Location getLocationHistory()
	{
		return locHistory;
	}
	
	public boolean getArchived()
	{
		return isArchived;
	}
	
	public boolean getLost()
	{
		return isLost;
	}
	
	public void setLocation(String coords, String date) throws Exception
	{
		locHistory.setLocation(coords, date);
	}

	public void updateLocation(String coords)
	{
		locHistory.updateLocation(coords);
	}

	public String locationsToFileString()
	{
		return this.locHistory.toFileString(this.itemID);
	}
	
	public void archive()
	{
		isArchived = true;
	}
	
	public void unArchive()
	{
		isArchived = false;
	}
	
	public void lost()
	{
		isLost = true;
	}
	
	public void found()
	{
		isLost = false;
	}
	
	public String toString()
	{
		return    "Item ID      : " + itemID +
				"\nItem Name    : " + itemName +
				"\nLast Location: " + locHistory.getLastLocation();
	}
	
	public String toFileString()
	{
		int lost, archived;
		if (this.isArchived == true)
			archived = 1;
		else
			archived = 0;
		
		if (this.isLost == true)
			lost = 1;
		else
			lost = 0;
		
		return this.itemID + "\n"
				+ this.userID + "\n"
				+ this.itemName + "\n"
				+ lost + "\n"
				+ archived + "\n";
	}
}