import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class jUnitTests {

	@Test
	void ReadTest() 
	{
		
	}
	
	@Test
	void CreateUserTest() 
	{
		User user = new User("Daniel", "345 Fake St.", "9165432109", "daniel@fake.com", "", 4);
		
		assertEquals("Daniel", user.getUserName());
		assertEquals("345 Fake St.", user.getAddress());
		assertEquals("9165432109", user.getPhoneNumber());
		assertEquals("daniel@fake.com", user.getEmail());
		assertEquals("", user.getContacts());
	}

}
