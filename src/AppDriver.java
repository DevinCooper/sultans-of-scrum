import java.util.ArrayList;
import java.util.Scanner;

public class AppDriver
{
	
	public static void main(String[] args) throws Exception
	{
		Server server = new Server();
		int userInput = 0;				//Used to track menu
		
		Scanner console = new Scanner(System.in);
		System.out.println("=====================================");
		System.out.println("|                                   |");
		System.out.println("|            F.I.N.D.O.             |");
		System.out.println("|  Found Item, Notify Device Owner  |");
		System.out.println("|                                   |");
		System.out.println("=====================================");
				
		while (userInput != 3)
		{
			System.out.println("");
			System.out.println("1. New User");
			System.out.println("2. Existing User");
			System.out.println("3. Exit");
			
			System.out.print("Enter a number: ");
			try
			{
				userInput = Integer.parseInt(console.nextLine());
				switch(userInput) 
				{
					case 1:
						NewUser(console, server);
						break;
					case 2:
						ExistingUser(console, server);
						break;
					case 3:
						server.logout();
						break;
					default:
						break;
				}
			}
			catch (Exception e)
			{
				System.out.println("Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
		System.out.println("=====================================");
		System.out.println("Thank you for using F.I.N.D.O");
		System.out.println("� Sultans of Scrum, INC");
		
	}

	private static void NewUser(Scanner console, Server server) 
	{
		String email, name, address, phoneNumber, otherContacts;
		
		System.out.println("You chose: 1. New User");
		System.out.println("=====================================");
		System.out.println("Create An Account");
		boolean done = false;
		
		while (!done)
		{
			System.out.print("Email: ");
			email = console.nextLine();
			if (!server.emailVerified(email))
			{
				System.out.print("Name: ");
				name = console.nextLine();
				System.out.print("Address: ");
				address = console.nextLine();
				System.out.print("Phone Number: ");
				phoneNumber = console.nextLine();
				System.out.print("Emergency Contact: ");
				otherContacts = console.nextLine();
				User user = server.newUser(name, address, phoneNumber, email, otherContacts);
				UserMenu(console, server, user);
				done = true;
			}
			else
			{
				System.out.println("This email is used in another account. Try again.");
			}
		}
	}
	
	private static void ExistingUser(Scanner console, Server server) 
	{
		System.out.println("You chose: 2. Existing User");
		System.out.println("=====================================");
		System.out.println("Logging Using An Existing Account");
		boolean attemptingLogin = true;
		
		while (attemptingLogin)
		{
			System.out.print("Enter email: ");
			String email = console.nextLine();
			if (server.emailVerified(email)) 
			{
				User user = server.userEmailSearch(email);
				System.out.println("Successfully logged in! Welcome " + user.getUserName());
				UserMenu(console, server, user);
				attemptingLogin = false;
			}
			else
				System.out.println("Email not found.");
		}
	}

	private static void UserMenu(Scanner console, Server server, User user)
	{
		System.out.println("=====================================");
		System.out.println("Welcome, " + user.getUserName());
		int userInput = 0;
		ArrayList<Item> userDevices = server.getUserItems(user.getUserID());
	
		while(userInput != 6) 
		{
			System.out.println("1. Add New Device");
			System.out.println("2. Remove Device");
			System.out.println("3. List My Devices");
			System.out.println("4. Report Lost Device");
			System.out.println("5. Found Lost Device");
			System.out.println("6. Logout");
			System.out.println("=====================================");
			System.out.print("Enter a number: ");
			try
			{
				userInput = Integer.parseInt(console.nextLine());
				System.out.print("You chose: ");
				switch(userInput)
				{
					case 1:
						System.out.println("1. Add New Device");
						System.out.print("Enter name of new device: ");
						String addedDevice = console.nextLine();
						server.addDevice(addedDevice, user);
						System.out.println("=====================================");
						break;
					case 2:
						System.out.println("2. Remove Device");
						PrintDevices(userDevices);
						System.out.print("Enter the number of the device you want to remove: ");
						int removedDevice = Integer.parseInt(console.nextLine());
						server.removeDevice(userDevices.get(removedDevice-1));
						System.out.println("=====================================");
						break;
					case 3:
						System.out.println("3. List Your Devices");
						userDevices = server.getUserItems(user.getUserID());
						PrintDevices(userDevices);
						System.out.println("=====================================");
						break;
					case 4:
						System.out.println("4. Report Device As Lost");
						userDevices = server.getUserItems(user.getUserID());
						PrintDevices(userDevices);
						System.out.print("Enter the number of the device you want to report lost: ");
						int lostDevice = Integer.parseInt(console.nextLine());
						server.reportLost(userDevices.get(lostDevice-1));
						System.out.println("=====================================");
						break;
					case 5: 
						System.out.println("5. Report Device As Found\n");
						ArrayList<Item> lostDevices = server.getLostUserItems(user.getUserID());
						PrintDevices(lostDevices);
						System.out.print("Enter the number of the device you want to report found: ");
						int foundDevice = Integer.parseInt(console.nextLine());
						server.reportFound(lostDevices.get(foundDevice-1));
						System.out.println("=====================================");
						break;
					case 6: 
						System.out.println("6. Logout.");
						System.out.println("=====================================");
						break;
					default:
						break;
				}
			}
			catch (Exception e) 
			{
				System.out.println("Error: " + e.getMessage());
			}
		}
	}
	
	private static void PrintDevices(ArrayList<Item> deviceList)
	{
		for (int i = 0; i < deviceList.size(); i++)
		{
			System.out.println((i+1) + ": " + deviceList.get(i).getItemName());
		}
	}

	
}

















