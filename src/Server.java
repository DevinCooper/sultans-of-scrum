import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

public class Server
{
	private ArrayList<Item> itemList;
	private ArrayList<User> userList;
	
	public Server() throws Exception
	{
		itemList = new ArrayList<>(0);
		userList = new ArrayList<>(0);
		
		readUsersFromFile();
		readItemsFromFile();
		readLocsFromFile();
	}
	
	public Server(boolean test)
	{
		itemList = new ArrayList<>(0);
		userList = new ArrayList<>(0);
	}
	
	public void logout()
	{
		writeUsersToFile();
		writeItemsToFile();
		writeLocsToFile();
		backupDatabase();
	}
	

	public HashMap<String, String> alertUser(User user, Item foundDevice)
	{
		String email = user.getEmail();
		String message = "Your device named " + foundDevice.getItemName() 
						+ " was last seen at " + foundDevice.getLocationHistory().getLastLocation();
		
		// sendEmail(email, message);	This would email the user.
		HashMap<String, String> alert = new HashMap<String, String>();
		alert.put(email, message);
		return alert;
	}
	
	public User newUser(String name, String addr, String phone, String email, String contacts)
	{
		userList.add(new User(name, addr, phone, email, contacts, userList.size()));
		return userList.get(userList.size()-1);
	}
	
	public void newUserFromFile(int id, String name, String addr, String phone, String email, boolean isArchived, String contacts)
	{
		userList.add(id, new User(name, addr, phone, email, contacts, id, isArchived));
	}
	
	public Item itemSearch(int id)
	{
		int low = 0;
		int high = itemList.size()-1;
		if (itemList.isEmpty())
			return null;
		else
		{
			while (low <= high)
			{
				int mid = (low + high) / 2;
				if (itemList.get(mid).getItemID() < id)
					low = mid + 1;
				else if (itemList.get(mid).getItemID() > id)
					high = mid - 1;
				else if (itemList.get(mid).getItemID() == id)
					return itemList.get(mid);
			}
		}
		return null;
	}
	
	public int itemIndexSearch(int id)
	{
		int low = 0;
		int high = itemList.size()-1;
		if (itemList.isEmpty())
			return -1;
		else
		{
			while (low <= high)
			{
				int mid = (low + high) / 2;
				if (itemList.get(mid).getItemID() < id)
					low = mid + 1;
				else if (itemList.get(mid).getItemID() > id)
					high = mid - 1;
				else if (itemList.get(mid).getItemID() == id)
					return mid;
			}
		}
		return -1;
	}
	
	public User userEmailSearch(String email)
	{
		for (User user : userList ) 
		{
			if (user.getEmail().equals(email)) return user;
				
		}
		return null;
	}
	
	public User userSearch(int id)
	{
		int low = 0;
		int high = userList.size()-1;
		if (userList.isEmpty())
			return null;
		else
		{
			while (low <= high)
			{
				int mid = (low + high) / 2;
				if (userList.get(mid).getUserID() < id)
					low = mid + 1;
				else if (userList.get(mid).getUserID() > id)
					high = mid - 1;
				else if (userList.get(mid).getUserID() == id)
					return userList.get(mid);
			}
		}
		return null;
	}

	public boolean emailVerified(String email)
	{
		if (userList.isEmpty())
			return false;
		for (int i = 0; i < userList.size(); i++)
		{
			if (email.equals(userList.get(i).getEmail()))
				return true;
		}
		
		return false;
	}
	
	public ArrayList<Item> getUserItems(int id)
	{
		ArrayList<Item> items = new ArrayList<>(0);
		if (itemList.isEmpty())
			return null;
		for (int i = 0; i < itemList.size(); i++)
		{
			if (id == itemList.get(i).getUserID() && itemList.get(i).getArchived() == false)
				items.add(itemList.get(i));
		}
		return items;
	}
	
	public ArrayList<Item> getLostUserItems(int id)
	{
		ArrayList<Item> items = new ArrayList<>(0);
		if (itemList.isEmpty())
			return null;
		for (int i = 0; i < itemList.size(); i++)
		{
			if (id == itemList.get(i).getUserID() && itemList.get(i).getArchived() == false && itemList.get(i).getLost() == true)
				items.add(itemList.get(i));
		}
		return items;		
	}
	
	public void addDevice(String name, User user)
	{
		itemList.add(new Item(name, itemList.size(), user.getUserID()));
	}

	public void addDeviceFromFile(int itemID, int userID, String name, boolean lost, boolean archived)
	{
		itemList.add(itemID, new Item(itemID, userID, name, lost, archived));
	}
	
	public void removeDevice(Item device)
	{
		device.archive();
	}
	
	public void reportLost(Item device)
	{
		device.lost();
	}
	
	public void reportFound(Item device)
	{
		device.found();
	}
	
	public void readUsersFromFile()
	{
		int id;
		String name, addr, phone, email, other, misc;
		boolean isArchived;
		
		try 
		{
			Scanner users = new Scanner(new File("users.txt"));
		
			while (users.hasNextLine())
			{
				id			= Integer.parseInt(users.nextLine());
				name		= users.nextLine();
				addr		= users.nextLine();
				phone		= users.nextLine();
				email		= users.nextLine();
				isArchived	= (Integer.parseInt(users.nextLine()) != 0);
				other		= users.nextLine();
				
				if (other != ",")
				{
					if (users.hasNextLine())
						misc = users.nextLine();
					else
						misc = ",";
					
					newUserFromFile(id, name, addr, phone, email, isArchived, other);
				}
				else
				{
					other = "";
					newUserFromFile(id, name, addr, phone, email, isArchived, other);
				}
				
			}
			users.close();
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void writeUsersToFile()
	{
		try 
		{
			FileWriter userFile = new FileWriter("users.txt");
			for (int i = 0; i < userList.size(); i++)
			{
				userFile.write(userList.get(i).toFileString());
			}
			userFile.close();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void readItemsFromFile()
	{
		int itemID, userID;
		String name;
		boolean isLost, isArchived;
		
		try
		{
			Scanner items = new Scanner(new File("items.txt"));
			
			while (items.hasNextLine())
			{
				itemID		= Integer.parseInt(items.nextLine());
				userID		= Integer.parseInt(items.nextLine());
				name		= items.nextLine();
				isLost		= (Integer.parseInt(items.nextLine()) != 0);
				isArchived	= (Integer.parseInt(items.nextLine()) != 0);
				
				addDeviceFromFile(itemID, userID, name, isLost, isArchived);
			}
			items.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	private void writeItemsToFile()
	{
		try 
		{
			FileWriter itemFile = new FileWriter("items.txt");
			for (int i = 0; i < itemList.size(); i++)
			{
				itemFile.write(itemList.get(i).toFileString());
			}
			itemFile.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void readLocsFromFile() throws Exception
	{
		int itemID;
		String coords, date;
		
		try 
		{
			Scanner locations = new Scanner(new File("locations.txt"));
			
			while (locations.hasNextLine())
			{
				itemID	= Integer.parseInt(locations.nextLine());
				coords	= locations.nextLine();
				date	= locations.nextLine();
				
				itemList.get(itemID).setLocation(coords, date);
			}
			locations.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	private void writeLocsToFile()
	{
		try 
		{
			FileWriter locations = new FileWriter("locations.txt");
			for (int i = 0; i < itemList.size(); i++)
			{
				locations.write(itemList.get(i).locationsToFileString());
			}
			locations.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	private void backupDatabase()
	{
		
		try {
			Files.copy(new File("users.txt").toPath(), new File("users_copy" + new SimpleDateFormat("yyyy-MM-dd hh-mm-ss'.txt'").format(new Date()) + ".txt").toPath());
			Files.copy(new File("items.txt").toPath(), new File("items_copy" + new SimpleDateFormat("yyyy-MM-dd hh-mm-ss'.txt'").format(new Date()) + ".txt").toPath());
			Files.copy(new File("locations.txt").toPath(), new File("locations_copy" + new SimpleDateFormat("yyyy-MM-dd hh-mm-ss'.txt'").format(new Date()) + ".txt").toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}










































