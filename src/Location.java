import java.util.Date;
import java.util.ArrayList;
import java.text.SimpleDateFormat;

public class Location
{
	private ArrayList<String> GPS;
	private ArrayList<Date> dates;
	
	public Location()
	{
		GPS = new ArrayList<>(0);
		dates = new ArrayList<>(0);
	}
	
	public void setLocation(String coords, String date) throws Exception
	{
		Date newDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(date);
		GPS.add(0, coords);
		dates.add(0, newDate);
	}
	
	public void updateLocation(String coords)
	{
		GPS.add(0, coords);
		dates.add(0, new Date());
	}
	
	public String getLastLocation()
	{
		return GPS.get(0) + dates.get(0).toString();
	}
	
	public String toFileString(int itemID)
	{
		String location = "";
		if (GPS.isEmpty())
			return location;
		for (int i = 0; i < GPS.size(); i++)
		{
			location += itemID + "\n"
					 + GPS.get(i) + "\n" 
					 + dates.get(i).toString() + "\n";
		}
		return location;
	}
}
