public class User
{
	private String name, address, phoneNumber, emailAddr, otherContacts;
	private int userID;
	private boolean isArchived;
	
	public User()
	{
		name = "";
		address = "";
		phoneNumber = "";
		emailAddr = "";
		userID = -1;
		otherContacts = "";
		isArchived = false;
	}
	
	public User(String name, String addr, String phone, String email, String contacts, int id)
	{
		this.name = name;
		address = addr;
		phoneNumber = phone;
		emailAddr = email;
	    otherContacts = contacts;
		userID = id;
		isArchived = false;
	}
	
	public User(String name, String addr, String phone, String email, String contacts, int id, boolean archived)
	{
		this.name = name;
		address = addr;
		phoneNumber = phone;
		emailAddr = email;
	    otherContacts = contacts;
		userID = id;
		isArchived = archived;
	}
	
	public String getUserName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
	
	public String getEmail()
	{
		return emailAddr;
	}
	
	public String getContacts()
	{
		return otherContacts;
	}
	
	public int getUserID()
	{
		return userID;
	}
	
	public boolean isArchived()
	{
		return isArchived;
	}
	
	public void setAddress(String addr)
	{
		address = addr;
	}
	
	public void setPhone(String phone)
	{
		phoneNumber = phone;
	}
	
	public void setEmail(String email)
	{
		emailAddr = email;
	}
	
	public void setContacts(String contacts)
	{
		otherContacts = contacts;
	}
	
	public void archive()
	{
		isArchived = true;
	}
	
	public String toString()
	{
		return 	"User ID	   : " + this.userID + "\n" +
				"Name		   : " + this.name + "\n" +
				"Address	   : " + this.address + "\n" +
				"Phone #	   : " + this.phoneNumber + "\n" +
				"Email  	   : " + this.emailAddr + "\n" +
				"Other Contact : " + this.otherContacts;
	}
	
	public String toFileString()
	{
		int archived;
		if (this.isArchived == true)
			archived = 1;
		else
			archived = 0;
		
		return this.userID + "\n"
				+ this.name + "\n"
				+ this.address + "\n"
				+ this.phoneNumber + "\n"
				+ this.emailAddr + "\n"
				+ archived + "\n"
				+ this.otherContacts + "\n"
				+ ",\n";
	}
}
